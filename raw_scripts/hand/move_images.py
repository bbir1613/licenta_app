import os
import cv2

from raw_scripts.hand.hand_recognition import HandRecognition

if __name__ == '__main__':
    test = 'test3'
    path = '../../assets/models/' + test
    hand = HandRecognition()
    for folder in os.listdir(path):
        images_path = path + '/' + folder
        for image_name in os.listdir(images_path):
            try:
                image_path = images_path + '/' + image_name
                image = cv2.imread(image_path)
                print("loaded image " + image_path)
                image = hand.crop_image(image)
                cv2.imwrite(image_path, image)
            except Exception:
                pass
