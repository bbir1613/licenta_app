import cv2
import numpy as np


class HandRecognition:
    def __init__(self):
        pass

    def __blur_image(self, image):
        '''        
        :param image: 
                Blur the image
                Convert to HSV color space
                Create a binary image with where white will be skin colors and rest is black
        :return: 
        '''
        blur = cv2.blur(image, (3, 3))
        hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)
        return cv2.inRange(hsv, np.array([2, 50, 50]), np.array([15, 255, 255]))

    def __determine_skin_area(self, mask2):
        '''
        :param mask2: 
                Kernel matrices for morphological transformation
                Perform morphological transformations to filter out the background noise
                Dilation increase skin color area
                Erosion increase skin color area
        :return: 
        '''
        kernel_square = np.ones((11, 11), np.uint8)
        kernel_ellipse = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
        dilation = cv2.dilate(mask2, kernel_ellipse, iterations=1)
        erosion = cv2.erode(dilation, kernel_square, iterations=1)
        dilation2 = cv2.dilate(erosion, kernel_ellipse, iterations=1)
        filtered = cv2.medianBlur(dilation2, 5)
        kernel_ellipse = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (8, 8))
        dilation2 = cv2.dilate(filtered, kernel_ellipse, iterations=1)
        kernel_ellipse = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
        _ = cv2.dilate(filtered, kernel_ellipse, iterations=1)
        median = cv2.medianBlur(dilation2, 5)
        return cv2.threshold(median, 127, 255, 0)

    def __find_the_max_area_contour(self, contours):
        '''        
        :param contours: 
                Find Max contour area (Assume that hand is in the image)
                Largest area contour
        :return: 
        '''
        max_area = 100
        ci = 0
        for i in range(len(contours)):
            cnt = contours[i]
            area = cv2.contourArea(cnt)
            if area > max_area:
                max_area = area
                ci = i

        return contours[ci]

    def crop_image(self, img):
        frame = img[:]
        mask2 = self.__blur_image(frame)
        ret, thresh = self.__determine_skin_area(mask2)
        _, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cnts = self.__find_the_max_area_contour(contours)
        # hull = cv2.convexHull(cnts)
        x, y, w, h = cv2.boundingRect(cnts)
        return img[y:y + h, x:x + w]
