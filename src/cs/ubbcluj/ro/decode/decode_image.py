import tensorflow as tf


class DecodeImage:
    def __init__(self, labels_path="../../../../assets/tf_files/retrained_labels.txt",
                 graph_path="../../../../assets/tf_files/retrained_graph.pb"):
        # Loads label file, strips off carriage return
        self.__label_lines = [line.rstrip() for line in tf.gfile.GFile(labels_path)]
        # Unpersists graph from file
        with tf.gfile.FastGFile(graph_path, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            _ = tf.import_graph_def(graph_def, name='')

    def decode_image(self, image_path):
        # Read in the image_data
        image_data = tf.gfile.FastGFile(image_path, 'rb').read()
        with tf.Session() as sess:
            # Feed the image_data as input to the graph and get first prediction
            softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
            predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})
            # Sort to show labels of first prediction in order of confidence
            top_k = predictions[0].argsort()
            node_id = top_k[-1]
            # return {'human_string': self.__label_lines[node_id], 'score': predictions[0][node_id]}
            return [self.__label_lines[node_id], predictions[0][node_id]]

# for node_id in top_k:
#         human_string = label_lines[node_id]
#         score = predictions[0][node_id]
# print('%s (score = %.5f)' % (human_string, score))
