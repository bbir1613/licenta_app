from src.cs.ubbcluj.ro.decode.decode_image import DecodeImage

if __name__ == "__main__":
    image_path = '../../../../raw_scripts/image/'
    decoder = DecodeImage()
    # print(decoder.decode_image(image_path+"A0.jpg"))
    cmd = str(input("give image : "))
    while cmd != 'exit':
        human_string, score = decoder.decode_image(image_path + cmd + ".jpg")
        print(human_string + " confidence: " + str(score))
        cmd = str(input("give image : "))
    print("Hello world")
