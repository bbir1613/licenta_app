from flask import Flask, request, jsonify
from werkzeug import secure_filename
import os
import cv2

from src.cs.ubbcluj.ro.decode.decode_image import DecodeImage
from src.cs.ubbcluj.ro.decode.hand_recognition import HandRecognition


class Route(Flask):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_url_rule('/', view_func=self.index, methods=['POST'])
        self.hand = HandRecognition()
        self.decode = DecodeImage()
        self.image_name = 'image.jpg'

    def index(self):
        f = request.files['file']
        f.save(secure_filename(f.filename))
        os.rename(f.filename, self.image_name)
        image = cv2.imread(self.image_name)
        image = self.hand.cropped_image(image)
        cv2.imwrite(self.image_name, image)
        cv2.imshow('image', image)
        cv2.waitKey(0)
        human_string, score = self.decode.decode_image(self.image_name)
        os.remove(self.image_name)
        return human_string
